package com.example.try_db_fragment_2;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyListFragment extends ListFragment {

	private OnItemSelectedListener listener;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

//		String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
//				"Blackberry", "WebOS", "Ubuntu", "Windows7", "Mac OS X",
//				"Linux", "OS/2" };

		// ArrayAdapter<String> adapter = new
		// ArrayAdapter<String>(getActivity(),
		// android.R.layout.simple_list_item_1, values);
		//
		// setListAdapter(adapter);

		MyAdapter adapter = new MyAdapter(ipsum.Headlines);
		setListAdapter(adapter);

	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		String item = (String) getListAdapter().getItem(position);
		updateDetail(position);
	}

	private class MyAdapter extends ArrayAdapter<String> {

		private String[] values;

		public MyAdapter(String[] values) {
			super(getActivity(), 0, values);
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// If we weren't given a view, inflate one
			if (convertView == null) {
				convertView = getActivity().getLayoutInflater().inflate(
						R.layout.fragment_rsslist_list, null);
			}

			// Configure the view for this Crime

			TextView title = (TextView) convertView.findViewById(R.id.label);

			title.setText(values[position]);

			return convertView;
		}
	}

	public interface OnItemSelectedListener {
		public void onRssItemSelected(int position);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof OnItemSelectedListener) {
			listener = (OnItemSelectedListener) activity;
		} else {
			throw new ClassCastException(activity.toString()
					+ " must implemenet MyListFragment.OnItemSelectedListener");
		}
	}

	// May also be triggered from the Activity
	public void updateDetail(int position) {
		// Send data to Activity
		listener.onRssItemSelected(position);
	}
}
